# How Puppet Finds Classes

Modern Puppet code is written so that every class lives in a separate
file within a module, and directories under `manifests` are used to
re-create the class hierarchy. Older Puppet code doesn't always follow
the rules, which can lead to confusion about where Puppet is finding
classes.  The examples here should help explain how Puppet finds these classes.

## How the Test Works

Each directory is a Puppet module directory containing a variant of the
`alice` module: each variant has the `alice::bob::secret` class defined
in a different manifest file (or not defined anywhere).

Each test runs `puppet-lint` over the corresponding module, then runs
`puppet apply` to test if the `alice::bob::secret` class can be found.
To simplify the setup, all the actual definitions are symlinks to
`secret.pp`; all the empty definitions are symlinks to `empty.pp`.

The tests can be listed:

```% make help
available targets: correct old halfway broken weird```

and tests can be run individually:

```% make correct
TESTING correct
Linting
OK
Applying
OK```

or all at once:

```%make
TESTING correct
Linting
OK
Applying
OK

TESTING old
Linting
ERROR: alice::bob::secret not in autoload module layout on line 2
Applying
OK

TESTING halfway
Linting
ERROR: alice::bob::secret not in autoload module layout on line 2
Applying
OK

TESTING broken
Linting
broken/alice/manifests/bob/secr3t.pp - ERROR: alice::bob::secret not in autoload module layout on line 2
Applying
Error: Evaluation Error: Error while evaluating a Function Call, Could not find class ::alice::bob::secret for animal.stanford.edu  at line 1:1 on node animal.stanford.edu

TESTING weird
Linting
weird/alice/manifests/bob.pp - ERROR: alice::bob::secret not in autoload module layout on line 2
Applying
OK```

## The Tests

### Missing

The `alice` module with no manifest files, therefore no `alice::bob::secret` class definition.

* LINT: should issue no errors or warnings
* CLASS: should issue a _Could not find class_ error.

### Misnamed

The `alice` module with the `alice::bob::secret` class definition in a misnamed file (`bob/bobsecret.pp`).

* LINT: should issue a _not in autoload module layout_ error.
* CLASS: should issue a _Could not find class_ error.

### Correct

The `alice` module in the correct, modern format.  This module has definitions for `alice`, `alice::bob`, and `alice::bob::secret`.

* LINT: should issue no errors or warnings.
* CLASS: should be found (in `bob/secret.pp`)

### Minimal

The `alice` module in the correct but minimal modern format.  This module only has a definition for `alice::bob::secret`.

* LINT: should pass
* CLASS: should be found

### Old

The `alice` module in an old format, with the `alice` and `alice::bob::secret` classes defined in `init.pp` (but no `alice::bob`).

* LINT: should fail (errors about _alice::bob::secret not in autoload module layout_)
* CLASS: should be found

### Halfway

The `alice` module in a half-modernized format, with the `alice::bob::secret` class defined in `bob.pp`.

* LINT: should fail (errors about _alice::bob::secret not in autoload module layout_)
* CLASS: should be found

### Duplicate

The `alice` module with the `alice::bob::secret` class defined in both `bob/secret.pp` and `bob.pp`.

* LINT: should fail (errors about _alice::bob::secret not in autoload module layout_)
* CLASS: should be found (in `bob/secret.pp`, not `bob.pp`)

### Triplicate

The `alice` module with the `alice::bob::secret` class defined in `bob/secret.pp`, `bob.pp`, and `init.pp`.

* LINT: should fail (errors about _alice::bob::secret not in autoload module layout_)
* CLASS: Puppet will issue a warning about multiple definitions (in `init.pp` and `bob/secret.pp`; but not `bob.pp`), then it will load the class from `init.pp` then `bob/secret.pp`, but not from `bob.pp`.

### Weird

The `alice` module in a half-modernized format, with the
`alice::bob::secret` class defined in `bob.pp`, but with an empty
`bob/secret.pp` file - just to show that while the modern structure is expected, Puppet will look in all the places

* LINT: should fail (errors about _alice::bob::secret not in autoload module layout_)
* CLASS: should be found (in `bob.pp`)
