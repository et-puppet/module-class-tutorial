# old/alice/manifests/init.pp

# alice should never be referenced
class alice {
  notify { 'OOPS! Found alice in init.pp':}
}

# no alice::bob

# alice::bob::secret
class alice::bob::secret {
  notify { 'Found alice::bob::secret in init.pp':}
}
