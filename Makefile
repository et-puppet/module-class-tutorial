TARGETS = missing misnamed old correct minimal halfway weird duplicate triplicate

all: $(TARGETS)

targets: 
	@echo available targets: $(TARGETS)

help: targets

$(TARGETS): .$@
	@echo TESTING $@
	@echo Linting
	@-puppet-lint $@/alice && echo OK || echo FAILED
	@echo Applying 
	@-puppet apply --modulepath=$@ -e 'include alice::bob::secret' | grep 'Notice: Found' && echo OK || echo FAILED
	@echo

