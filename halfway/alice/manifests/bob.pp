# halfway/alice/manifests/bob.pp

# alice::bob
class alice::bob {
  notify { 'Found alice::bob in bob.pp':}
}

# alice::bob::secret
class alice::bob::secret {
  notify { 'Found alice::bob::secret in bob.pp':}
}
