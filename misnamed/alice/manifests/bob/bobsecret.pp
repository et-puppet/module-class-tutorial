# misnamed/alice/manifests/bob/bobsecret.pp

# should never be found
class alice::bob::secret {
  notify { 'Found alice::bob::secret in bob/bobsecret.pp':}
}

# should never be referenced
class alice::bob::bobsecret {
  notify { 'OOPS! Found alice::bob::bobsecret in bob/bobsecret.pp':}
}

